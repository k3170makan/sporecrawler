#!/usr/bin/python3
import angr
import random
import argparse


class nDFS(angr.ExplorationTechnique):
	"""
	Depth-first search with configurable alive-path limit
	
	Will only keep n paths active at a time, any others will be stashed in the 'deferred' stash.
	When we run out of active paths to step, we take the longest one from deferred and continue.
	"""
	def __init__(self, limit=1,deferred_stash='deferred'):
		super(nDFS, self).__init__()
		self.limit = limit
		self._random = random.Random()
		self._random.seed(10)
		self.deferred_stash = deferred_stash

	def setup(self, simgr):
		if self.deferred_stash not in simgr.stashes:
			simgr.stashes[self.deferred_stash] = []
	def step(self, simgr, stash='active', **kwargs):
		simgr = simgr.step(stash=stash, **kwargs)
		if len(simgr.stashes[stash]) > 1:
			self._random.shuffle(simgr.stashes[stash])
			simgr.split(from_stash=stash, to_stash=self.deferred_stash, limit=self.limit)
			
		if len(simgr.stashes[stash]) == 0:
			if len(simgr.stashes[self.deferred_stash]) == 0:
				return simgr
		return simgr

class DivertExecuteAddressGoal(angr.exploration_techniques.director.BaseGoal):
	def __init__(self, addr):
		super(DivertExecuteAddressGoal,self).__init__('diverted_execute_address')
		self.addr = addr
	def __repr__(self):
		return "<DivertExecuteAddressGoal targeting %#x>" % self.addr #one goal for each address

	def check(self, cfg, state, peek_blocks):
		"""we are diverting from a goal in order to discover more access points"""

		node = self._get_cfg_node(cfg,state)
		if node is None:
			#l.error('Failed to find CFGNode for state %s on the control flow graph.', state)
			return False

		#not sure if this simple algorithm will do, might need to speed this up
		for src,dst in self._dfs_edges(cfg.graph, node, max_steps=peek_blocks):
			if src.addr == self.addr or dst.addr == self.addr:
				#print("{dea> State %s goal [%s] will reach, diverting..." % (state, hex(self.addr)))
				#l.debug('State %s will not reach %#x.', state, self.addr)	
				return False

		#l.debug('SimState %s will not reach %#x.', state, self.addr)		
		#print("{dea> State %s will not reach goal [%s] stepping..." % (state, hex(self.addr)))
		return True

	def check_state(self, state):
		return state.addr != self.addr
