#!/usr/bin/python3
import angr
import random
import sys
import claripy
import argparse
import logging

logging.getLogger('angr').setLevel('CRITICAL')

memory_access = [] 
_sym_stdin = None
_args = []
_sym_files = 0
_hooks = 0
_hooks_tainted = 0

REGNAMES = ["rip","rax","rbx","rcx","rdx","rsi","rdi","rbp","rsp","r9","r8""r10","r11","r12","r13","r14","r15","cs","ds","es","fs","gs","eflags"]

"""
	SporeCrawler essentially does taint tracking using angr and claripy's annotations
"""
class nDFS(angr.ExplorationTechnique):
	"""
	Depth-first search with configurable alive-path limit
	
	Will only keep n paths active at a time, any others will be stashed in the 'deferred' stash.
	When we run out of active paths to step, we take the longest one from deferred and continue.
	"""
	def __init__(self, limit=1,deferred_stash='deferred'):
		super(nDFS, self).__init__()
		self.limit = limit
		self._random = random.Random()
		self._random.seed(10)
		self.deferred_stash = deferred_stash

	def setup(self, simgr):
		if self.deferred_stash not in simgr.stashes:
			simgr.stashes[self.deferred_stash] = []
	def step(self, simgr, stash='active', **kwargs):
		simgr = simgr.step(stash=stash, **kwargs)
		if len(simgr.stashes[stash]) > 1:
			self._random.shuffle(simgr.stashes[stash])
			simgr.split(from_stash=stash, to_stash=self.deferred_stash, limit=self.limit)
			
		if len(simgr.stashes[stash]) == 0:
			if len(simgr.stashes[self.deferred_stash]) == 0:
				return simgr
		return simgr

class DivertExecuteAddressGoal(angr.exploration_techniques.director.BaseGoal):
	def __init__(self, addr):
		super(DivertExecuteAddressGoal,self).__init__('diverted_execute_address')
		self.addr = addr
	def __repr__(self):
		return "<DivertExecuteAddressGoal targeting %#x>" % self.addr #one goal for each address

	def check(self, cfg, state, peek_blocks):
		"""we are diverting from a goal in order to discover more access points"""

		node = self._get_cfg_node(cfg,state)
		if node is None:
			#l.error('Failed to find CFGNode for state %s on the control flow graph.', state)
			return False

		#not sure if this simple algorithm will do, might need to speed this up
		for src,dst in self._dfs_edges(cfg.graph, node, max_steps=peek_blocks):
			if src.addr == self.addr or dst.addr == self.addr:
				#print("{dea> State %s goal [%s] will reach, diverting..." % (state, hex(self.addr)))
				#l.debug('State %s will not reach %#x.', state, self.addr)	
				return False

		#l.debug('SimState %s will not reach %#x.', state, self.addr)		
		#print("{dea> State %s will not reach goal [%s] stepping..." % (state, hex(self.addr)))
		return True

	def check_state(self, state):
		return state.addr != self.addr

def _eval_bytes(state,_val):
	if (type(_val) != type(bytes)):
		try:
			_eval = state.solver.eval(_val,cast_to=bytes)
			return _eval
		except:
			pass
	return _val

def _eval_int(state,_val):
	if (type(_val) != type(1)):
		try:
			_eval = state.solver.eval(_val,cast_to=int)
			return _eval
		except:
			pass
	elif type(_eval) == type("a") and _eval[:2] == "0x":
		return int(_eval,16)
	return _val

def _add_access(addr):
	if (type(addr) != int):
		raise Exception("[x] _add_access: addr not integer")
	if not(addr in memory_access):
		memory_access.append(addr)

def __print_history(state):
	summary = False
	if len(list(state.history.parents)) > 10:
		summary = True
	history = list(state.history.parents)
	history_length = len(history)
	print("\t\thistory [%s]:" % (history_length))
	for index,state in enumerate(history):
		if (index < 3 and summary):
			print("\t\t\t%s" % (state))
		if (index == history_length-5):
			print("\t\t\t...")
		if (index > history_length-5):
			print("\t\t\t%s" % (state))

def _print_constraints(state):
	for constraint in state.solver.constraints:
		if "cmd_arg" in repr(constraint):
			print ("\t*- { %s... }" % (repr(constraint)[:90]))
		else:
			print ("\t-- { %s... }" % (repr(constraint)[:70]))

def is_tainted(state):
	_tainted_regs = []
	if state.regs.rsi.to_claripy().annotations != None:
		_tainted_regs.append("rsi")
	if state.regs.rdi.to_claripy().annotations != None:
		_tainted_regs.append("rdi")
	if state.regs.rsi.to_claripy().annotations != None:
		_tainted_regs.append("rsi")
	if state.regs.rbp.to_claripy().annotations != None:
		_tainted_regs.append("rbp")
	if state.regs.rsp.to_claripy().annotations != None:
		_tainted_regs.append("rsp")
	if state.regs.rip.to_claripy().annotations != None:
		_tainted_regs.append("rip")
	if state.regs.rax.to_claripy().annotations != None:
		_tainted_regs.append("rax")
	if state.regs.rbx.to_claripy().annotations != None:
		_tainted_regs.append("rbx")
	if state.regs.rcx.to_claripy().annotations != None:
		_tainted_regs.append("rcx")
	if state.regs.rdx.to_claripy().annotations != None:
		_tainted_regs.append("rdx")
	if state.regs.r10.to_claripy().annotations != None:
		_tainted_regs.append("r10")
	if state.regs.r12.to_claripy().annotations != None:
		_tainted_regs.append("r12")
	if state.regs.r13.to_claripy().annotations != None:
		_tainted_regs.append("r13")
	if state.regs.r14.to_claripy().annotations != None:
		_tainted_regs.append("r14")
	if state.regs.r15.to_claripy().annotations != None:
		_tainted_regs.append("r15")

	return _tainted_regs
def eval_args(state):

	_regs = is_tainted(state)
	for index,arg in enumerate(_args):
		print("\t\t|--- arg_%s :=> %s " % (index,state.solver.eval(arg, cast_to=bytes)))
	if (_regs != None) and len(_regs) > 0:
		print("\t\t|--- tainted regs : ",end="")
		for reg in _regs:
			print("%s " % (reg),end="")
		print()
	print("\t|-- satisfiable : %s " % (state.satisfiable()))
	print("\t\t|--- stdin :=> %s " % (state.solver.eval(_sym_stdin, cast_to=bytes)))
	print()

	
class fopen_hook(angr.SimProcedure):
	def run(self, path_name, mode):
		#_path_name = _eval_bytes(self.state, path_name.to_claripy())
		#_path_name = self.state.solver.eval(self.state.memory.load(_path_name,32),cast_to=bytes)
		#_path_name = self.state.solver.eval(path_name.to_claripy(),cast_to=int)
		_mode = _eval_int(self.state, mode.to_claripy())
		
		if(is_tainted(self.state) != None):# and not(_eval_int(self.state,self.state.ip) in memory_access)):
			
			new_path_name = _eval_bytes(self.state, self.state.memory.load(path_name,64))
		
			print("|--[%s]> fopen (path_name=[%s...], mode=[%s])" % (self.state,
							repr(new_path_name)[:30],
							repr(_mode)[:30]))
				
			#memory_access.append(_eval_int(self.state,self.state.ip))
			#return claripy.BVS("sym_file_%s" % (repr(int(1+random.random()*9999%10))),0x8*128,annotations=(claripy.Annotation(),))


			print(type(self.state.ip.to_claripy()))
			eval_args(self.state)
	
class strncpy_hook(angr.SimProcedure):
	def run(self, dest, src, n):
		_dest = dest
		_src = src
		_n = n

		try:
			_dest = hex(_eval_int(self.state, dest.to_claripy()))
			_src = _eval_int(self.state, src.to_claripy())
			_n = _eval_int(self.state, n.to_claripy())
			
			_src = _eval_bytes(self.state, 
						self.state.memory.load(_src,
									32))
			
		except:
			pass

		if(is_tainted(self.state) != None):# and not(_eval_int(self.state, self.state.ip) in memory_access)):
			print("|--[%s]> strncpy (dest=[%s], src=[%s...],  n=[%s])" % (repr(self.state),
					repr(_dest)[:30],
					repr(_src)[:30],
					repr(_n)[:30]))
				


			eval_args(self.state)
			if type(self.state.ip) != type(claripy.BVS("a",1)):
				_add_access(_eval_int(self.state,self.state.ip.to_claripy()))
			else:
				_add_access(_eval_int(self.state,self.state.ip))


			new_dest = claripy.BVS("hooked_arg_%s" % ((random.random()*999%64)), 8,annotations=(claripy.Annotation(),))
			return dest 

class strcpy_hook(angr.SimProcedure):
	def run(self, dest, src):
		_src = src
		_dest = dest
		try:
			_src = _eval_int(self.state, src.to_claripy())
			_dest = hex(_eval_int(self.state, dest.to_claripy()))
			_src = _eval_bytes(self.state.memory.load(src,32),cast_to=bytes)

		except:
			pass

		if(is_tainted(self.state) != None):# and not(_eval_int(self.state, self.state.ip) in memory_access)):
			print("|--[%s]> strcpy (dest=[%s], src=[%s...])" % (repr(self.state),
								repr(_dest)[:30],
								repr(_src)[:30]))
			

			eval_args(self.state)
	
			#_print_constraints(self.state)
			#memory_access.append(_eval_int(self.state,self.state.ip))
			if type(self.state.ip) != type(claripy.BVS("a",1)):
				_add_access(_eval_int(self.state,self.state.ip.to_claripy()))
			else:
				_add_access(_eval_int(self.state,self.state.ip))

			dest = claripy.BVS("hooked_arg_%s" % ((random.random()*999%64)), 0x8,annotations=(claripy.Annotation(),))
			return dest 
		


class memmove_hook(angr.SimProcedure):
	def run(self, dest, src, n):

		_dest = dest
		_src = src
		_n = n

		try:
			#_dest = _eval_int(self.state, dest.to_claripy())
			#_src = _eval_int(self.state, src.to_claripy())

			_n = _eval_int(self.state, n.to_claripy())
			_src = _eval_int(self.state, src.to_claripy())
			_dest = hex(_eval_int(self.state, dest.to_claripy()))
			_src = _eval_bytes(self.state.memory.load(src,32),cast_to=bytes)

			#_src = _eval_int(self.state, 
			#			self.state.memory.load(_src,
			#						_n))
		except:
			pass

		if(is_tainted(self.state) != None):# and not(_eval_int(self.state, self.state.ip) in memory_access)):
			print("|--[%s]> memmove (dest=[%s], src=[%s...], n=[%s])" % (repr(self.state),
							repr(_dest)[:30],
							repr(_src)[:30],
							repr(_n)[:30]))			
					

			eval_args(self.state)
			if type(self.state.ip) != type(claripy.BVS("a",1)):
				_add_access(_eval_int(self.state,self.state.ip.to_claripy()))
			else:
				_add_access(_eval_int(self.state,self.state.ip))

			dest = claripy.BVS("hooked_arg_%s" % ((random.random()*999%64)), 8,annotations=(claripy.Annotation(),))
			return dest 
		

class memset_hook(angr.SimProcedure):
	def run(self, s, c, n):
		_s = s
		try:
			_n = _eval_int(self.state, n.to_claripy())
			_s = self.state.solver.eval(self.state.memory.load(src,32),cast_to=bytes)

		except:
			pass
		if(is_tainted(self.state) != None):# and not(_eval_int(self.state, self.state.ip) in memory_access)):
			print("|--[%s]> memset (s=[%s], c=[%s], n=[%s])" % (repr(self.state),
									repr(_s)[:30],
									repr(c)[:30],
									repr(_n)[:30]))

				
			eval_args(self.state)
			if type(self.state.ip) != type(claripy.BVS("a",1)):
				_add_access(_eval_int(self.state,self.state.ip.to_claripy()))
			else:
				_add_access(_eval_int(self.state,self.state.ip))

			dest = claripy.BVS("hooked_arg_%s" % ((random.random()*999%64)), 8,annotations=(claripy.Annotation(),))
			return dest 


class memcpy_hook(angr.SimProcedure):
	def run(self, dest, src, n):
		_dest = dest
		_src = src
		_n = n

		try:
			_dest = _eval_int(self.state, dest.to_claripy())
			_src = _eval_int(self.state, src.to_claripy())
			_n = _eval_int(self.state, n.to_claripy())

			#_src = _eval_int(self.state, 
			#			self.state.memory.load(_src,
			#						_n))

			_src = _eval_bytes(self.state.memory.load(src,32),cast_to=bytes)

		except:
			pass

		if(is_tainted(self.state) != None):# not(_eval_int(self.state, self.state.ip) in memory_access)):
			print("|--[%s]> memcpy (dest=[%s], src=[%s...], n=[%s])" % (repr(self.state),
									repr(_dest)[:30],
									repr(_src)[:30],
									repr(_n)[:30]))			

				
			eval_args(self.state)
			if type(self.state.ip) != type(claripy.BVS("a",1)):
				_add_access(_eval_int(self.state,self.state.ip.to_claripy()))
			else:
				_add_access(_eval_int(self.state,self.state.ip))

			dest = claripy.BVS("hooked_arg_%s" % ((random.random()*999%64)), 
								8,
							annotations=(claripy.Annotation(),))
			return dest 
	
"""
class fscanf_hook():
class fgets_hook():
class sprintf_hook():
class snprintf_hook():
class vsprintf_hook():
class vnsprintf_hook():
class getch_hook():
getchar",getchar_hook())	
strcpy",strcpy_hook())	
strncpy",strncpy_hook())	
strlen",strlen_hook())	
strcat",strcat_hook())	
strncat",strncat_hook())	
strcmp",strcmp_hook())	
memcpy",memcpy_hook())	
memcmp",memcmp_hook())	
memmove",memmove_hook())	
memset",memset_hook())	
"""


class SporeCrawler(angr.Analysis):
	def __init__(self,filename=None,initial_state=None,project=None,steps=500,use_dm=False,limit=1):

		self.use_dm = use_dm
		self.limit = limit
		self.filename = filename
		self.steps = steps
		self.project = project
		self.goals = []
		self.deadends = []
		self.target_functions = []
		self.found_targets = False
		self.initial_state = initial_state
		self.CFG_done = False
		self.using_dm = False
		self.diverted = False
		self._init_hooks()
		self._init_target_functions()
		self.dm = angr.exploration_techniques.Director(cfg_keep_states=True)

		self.simmgr = project.factory.simulation_manager(self.initial_state)

		self.simmgr.use_technique(nDFS(self.limit))
		#self.simmgr.use_technique(angr.exploration_techniques.DFS())
		if self.use_dm:
			#print("*> using directed symbolic exploration towards => %s" % (self.target_functions))
			try:
				print("[*] deriving CFG...",end='\r')
				self.project.analyses.CFGFast()
				self.CFG_done = True
				print("[*] CFG derivation successfull!")
			except:
				print("[x] CFGFast failed...")
				self.CFG_done = False

			self._init_goals()
			self.simmgr.use_technique(self.dm)

		#crawler = ["_","_/","_/-","_/--","_/---","_/---\\","/---\\_","---\\_","--\\_","-\\_","\\_"]
		for i in range(steps):
		#try:
			#print("[%s>] %d/%d {%s} (%s) %s -%s-" % (self.filename,
			try:
				print("[%s>] {%s} (%s) %s -%s-" % (self.filename,
							#_hooks_tainted,_hooks,
							i,len(self.simmgr.stashes['active']),
							self.simmgr.stashes['active'][0],
							len(self.simmgr.stashes['deadended'])),end='\r')

				self.simmgr.step()
			except:
				pass
			if memory_access != []:
				if not(self.CFG_done):
					self.project.analyses.CFGFast()
					self.CFG_done = True
				"""
				for deadend in self.simmgr.stashes['deadended']:
					ip = deadend.ip
					if type(ip) != int or type(ip) != type(claripy.BVS()):
							ip = _eval_int(deadend,ip)
					if not(ip in self.deadends):
						self.deadends.append(ip)
						self.append_goal(ip)
				"""

				for state_ip in memory_access:
					if not(state_ip in self.goals):
						self.append_goal(state_ip)				
				
				if self.use_dm and not(self.using_dm):
					print("!> switching to directed symbolic execution...")
					self.simmgr.use_technique(self.dm) #this won't work we need to dynamically add goals
					self.using_dm = True
	
				if len(self.simmgr.stashes['active']) > 1024:
					break
			#except Exception as e:
				#print(e)
	
	
	
	def _init_hooks(self):
		self.project.hook_symbol("fopen",fopen_hook())	
		#self.project.hook_symbol("fscanf",fscanf_hook())	
		#self.project.hook_symbol("fgets",fgets_hook())	
		#self.project.hook_symbol("sprintf",sprintf_hook())	
		#self.project.hook_symbol("snprintf",snprintf_hook())	
		#self.project.hook_symbol("vsprintf",vsprintf_hook())	
		#self.project.hook_symbol("vnsprintf",vnsprintf_hook())	
		#self.project.hook_symbol("getch",getch_hook())	
		#self.project.hook_symbol("getchar",getchar_hook())	
		self.project.hook_symbol("strcpy",strcpy_hook())	
		self.project.hook_symbol("strncpy",strncpy_hook())	
		#self.project.hook_symbol("strlen",strlen_hook())	
		#self.project.hook_symbol("strcat",strcat_hook())	
		#self.project.hook_symbol("strncat",strncat_hook())	
		#self.project.hook_symbol("strcmp",strcmp_hook())	
		self.project.hook_symbol("memcpy",memcpy_hook())	
		#self.project.hook_symbol("memcmp",memcmp_hook())	
		self.project.hook_symbol("memmove",memmove_hook())	
		self.project.hook_symbol("memset",memset_hook())	


		self.target_function_names = ['strcpy','memcpy','strncpy','memmove','memset']

	def _init_target_functions(self):
		for function_name in self.target_function_names:
			_function = self.project.kb.functions.function(name=function_name,plt=False)
			if _function:
				print("*> found function => %s @[%s]" % (function_name, _function))	
				self.target_functions.append(_function.addr)

		if len(self.target_functions) > 0:
			self.found_targets = True	


	def append_goal(self,addr):
		print("*>\t adding goal => %s [%s]" % (type(addr),hex(addr)))
		#_goal = DivertExecuteAddressGoal(addr)
		_goal = angr.exploration_techniques.ExecuteAddressGoal(addr)
		self.goals.append(addr) #goals we are already diverting from
		if self.diverted:
			self.dm.add_goal(_goal)
		else:
			self.dm = angr.exploration_techniques.Director(cfg_keep_states=True, goals=[_goal])
			self.diverted = True

	def _init_goals(self):
		for function in self.target_functions:
			_goal = angr.exploration_techniques.ExecuteAddressGoal(function.addr)
			self.goals.append(_goal)	
			self.dm.add_goal(_goal)


angr.AnalysesHub.register_default("SporeCrawler",SporeCrawler)

class TaintAnnotation(claripy.Annotation):
    def eliminatable(self):
        return False

    def relocatable(self):
        return False


if __name__=="__main__":

	parser = argparse.ArgumentParser()
	
	parser.add_argument("-D","--directed_exec",help="Use directed symbolic execution",action='store_true',default=False)	

	parser.add_argument("-b","--binary",help="Binary to parse")	
	parser.add_argument("-c","--arg_count",help="Number of arguments",default=3)	
	parser.add_argument("-l","--arg_length",help="Bit length of each argument",default=16)	
	parser.add_argument("-L","--limit",help="maximum number of states to keep alive during depth first search",type=int,default=1)	
	parser.add_argument("-s","--step_count",help="Number of symbolic execution steps",default=256)	
	parser.add_argument("--stdin",help="symbolize stdin, argument is the length of stdin as a byte-wise claripy.BVS",default=64,type=type(1))	
	args = parser.parse_args()

	binary_name = args.binary
	arg_count = int(args.arg_count)
	arg_length = int(args.arg_length)
	step_count = int(args.step_count)

	project = angr.Project(binary_name,load_options={"auto_load_libs":False,"main_opts":{"base_addr":0x400000, "force_rebase":False}});
	
	print("|--binary :=> %s" % (binary_name))
	print("|--arg_count :=> %s" % (arg_count))
	print("|--arg_length :=> %s" % (arg_length))

	_args = [claripy.BVS("cmd_arg%s" % (i), 8*arg_length,annotations=(claripy.Annotation(),)) for i in range(arg_count)]
	_sym_stdin = claripy.BVS("sym_stdin", 8*args.stdin,annotations=(claripy.Annotation(),))
	entry_state = project.factory.entry_state(args=_args,stdin=_sym_stdin)
	global_address_access = project.analyses.SporeCrawler(filename=binary_name,
									initial_state=entry_state,
									project=project,
									steps=step_count,
									use_dm=args.directed_exec,
									limit=args.limit)	
