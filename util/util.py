#!/usr/bin/python3
import angr
import claripy

REGNAMES = ["rip","rax","rbx","rcx","rdx","rsi","rdi","rbp","rsp","r9","r8""r10","r11","r12","r13","r14","r15","cs","ds","es","fs","gs","eflags"]

def _eval_bytes(state,_val):
	if (type(_val) != type(bytes)):
		try:
			_eval = state.solver.eval(_val,cast_to=bytes)
			return _eval
		except:
			pass
	return _val

def _eval_int(state,_val):
	if (type(_val) != type(1)):
		try:
			_eval = state.solver.eval(_val,cast_to=int)
			return _eval
		except:
			pass
	elif type(_eval) == type("a") and _eval[:2] == "0x":
		return int(_eval,16)
	return _val

def _add_access(addr,array):
	if (type(addr) != int):
		raise Exception("[x] _add_access: addr not integer")
	if not(addr in array):
		array.append(addr)

def __print_history(state):
	summary = False
	if len(list(state.history.parents)) > 10:
		summary = True
	history = list(state.history.parents)
	history_length = len(history)
	print("\t\thistory [%s]:" % (history_length))
	for index,state in enumerate(history):
		if (index < 3 and summary):
			print("\t\t\t%s" % (state))
		if (index == history_length-5):
			print("\t\t\t...")
		if (index > history_length-5):
			print("\t\t\t%s" % (state))

def _print_constraints(state):
	for constraint in state.solver.constraints:
		if "cmd_arg" in repr(constraint):
			print ("\t*- { %s... }" % (repr(constraint)[:90]))
		else:
			print ("\t-- { %s... }" % (repr(constraint)[:70]))

def is_tainted(state):
	_tainted_regs = []
	if state.regs.rsi.to_claripy().annotations != None:
		_tainted_regs.append("rsi")
	if state.regs.rdi.to_claripy().annotations != None:
		_tainted_regs.append("rdi")
	if state.regs.rsi.to_claripy().annotations != None:
		_tainted_regs.append("rsi")
	if state.regs.rbp.to_claripy().annotations != None:
		_tainted_regs.append("rbp")
	if state.regs.rsp.to_claripy().annotations != None:
		_tainted_regs.append("rsp")
	if state.regs.rip.to_claripy().annotations != None:
		_tainted_regs.append("rip")
	if state.regs.rax.to_claripy().annotations != None:
		_tainted_regs.append("rax")
	if state.regs.rbx.to_claripy().annotations != None:
		_tainted_regs.append("rbx")
	if state.regs.rcx.to_claripy().annotations != None:
		_tainted_regs.append("rcx")
	if state.regs.rdx.to_claripy().annotations != None:
		_tainted_regs.append("rdx")
	if state.regs.r10.to_claripy().annotations != None:
		_tainted_regs.append("r10")
	if state.regs.r12.to_claripy().annotations != None:
		_tainted_regs.append("r12")
	if state.regs.r13.to_claripy().annotations != None:
		_tainted_regs.append("r13")
	if state.regs.r14.to_claripy().annotations != None:
		_tainted_regs.append("r14")
	if state.regs.r15.to_claripy().annotations != None:
		_tainted_regs.append("r15")

	return _tainted_regs
def eval_args(state):

	_regs = is_tainted(state)
	for index,arg in enumerate(_args):
		print("\t\t|--- arg_%s :=> %s " % (index,state.solver.eval(arg, cast_to=bytes)))
	if (_regs != None) and len(_regs) > 0:
		print("\t\t|--- tainted regs : ",end="")
		for reg in _regs:
			print("%s " % (reg),end="")
		print()
	print("\t|-- satisfiable : %s " % (state.satisfiable()))
	print("\t\t|--- stdin :=> %s " % (state.solver.eval(_sym_stdin, cast_to=bytes)))
	print()
