* Add unique print option -u
* Make strings inspection plugin -S
* Implement info level printing
* Add string print out length option --string-length
* Add strings option, where just the strings are returned -s
* Add other exploration methods --explore [algorithm name] / [plugin script]
* Finish GDB Plugins
* Better handling of angr based execptions
* Fix Up hooks (need abstracted class)
* neater output (remove ascii art stuff)
* Binary Ninja Plugins
* Windows Executable Support
* Interactive Shell
