#!/usr/bin/python3
import angr
import claripy
import random

import sys
sys.path.append("..")

from util import *
import _global_variables

REGNAMES = ["rip","rax","rbx","rcx","rdx","rsi","rdi","rbp","rsp","r9","r8""r10","r11","r12","r13","r14","r15","cs","ds","es","fs","gs","eflags"]

def _eval_bytes(state,_val):
	if (type(_val) != type(bytes)):
		try:
			_eval = state.solver.eval(_val,cast_to=bytes)
			return _eval
		except:
			pass
	return _val

def _eval_int(state,_val):
	if (type(_val) != type(1)):
		try:
			_eval = state.solver.eval(_val,cast_to=int)
			return _eval
		except:
			pass
	elif type(_eval) == type("a") and _eval[:2] == "0x":
		return int(_eval,16)
	return _val

def _add_access(addr):
	if (type(addr) != int):
		raise Exception("[x] _add_access: addr not integer")
	if not(addr in _global_variables.memory_access):
		_global_variables.memory_access.append(addr)

def __print_history(state):
	summary = False
	if len(list(state.history.parents)) > 10:
		summary = True
	history = list(state.history.parents)
	history_length = len(history)
	print("\t\thistory [%s]:" % (history_length))
	for index,state in enumerate(history):
		if (index < 3 and summary):
			print("\t\t\t%s" % (state))
		if (index == history_length-5):
			print("\t\t\t...")
		if (index > history_length-5):
			print("\t\t\t%s" % (state))

def _print_constraints(state):
	for constraint in state.solver.constraints:
		if "cmd_arg" in repr(constraint):
			print ("\t*- { %s... }" % (repr(constraint)[:90]))
		else:
			print ("\t-- { %s... }" % (repr(constraint)[:70]))

def is_tainted(state):
	_tainted_regs = []
	if state.regs.rsi.to_claripy().annotations != None:
		_tainted_regs.append("rsi")
	if state.regs.rdi.to_claripy().annotations != None:
		_tainted_regs.append("rdi")
	if state.regs.rsi.to_claripy().annotations != None:
		_tainted_regs.append("rsi")
	if state.regs.rbp.to_claripy().annotations != None:
		_tainted_regs.append("rbp")
	if state.regs.rsp.to_claripy().annotations != None:
		_tainted_regs.append("rsp")
	if state.regs.rip.to_claripy().annotations != None:
		_tainted_regs.append("rip")
	if state.regs.rax.to_claripy().annotations != None:
		_tainted_regs.append("rax")
	if state.regs.rbx.to_claripy().annotations != None:
		_tainted_regs.append("rbx")
	if state.regs.rcx.to_claripy().annotations != None:
		_tainted_regs.append("rcx")
	if state.regs.rdx.to_claripy().annotations != None:
		_tainted_regs.append("rdx")
	if state.regs.r10.to_claripy().annotations != None:
		_tainted_regs.append("r10")
	if state.regs.r12.to_claripy().annotations != None:
		_tainted_regs.append("r12")
	if state.regs.r13.to_claripy().annotations != None:
		_tainted_regs.append("r13")
	if state.regs.r14.to_claripy().annotations != None:
		_tainted_regs.append("r14")
	if state.regs.r15.to_claripy().annotations != None:
		_tainted_regs.append("r15")

	return _tainted_regs
def eval_args(state):
	_regs = is_tainted(state)
	#commenting out the arg evaluation util i have a fix
	for index,arg in enumerate(_global_variables._sym_args):
		print("\t\t-- arg_%s : %s " % (index,state.solver.eval(arg, cast_to=bytes)))
	#if (_regs != None) and len(_regs) > 0:
	#	print("\t\t|--- tainted regs : ",end="")
	#	for reg in _regs:
	#		print("%s " % (reg),end="")
	#	print()
	#print("\t-- stdin : %s" % (state.solver.eval(_sym_stdin, cast_to=bytes)))
	print("\t-- satisfiable : %s" % (state.satisfiable()))

class strncpy_hook(angr.SimProcedure):
	def run(self, dest, src, n):
		_dest = dest
		_src = src
		_n = n

		try:
			_dest = hex(_eval_int(self.state, dest.to_claripy()))
			_src = _eval_int(self.state, src.to_claripy())
			_n = _eval_int(self.state, n.to_claripy())
			
			_src = _eval_bytes(self.state, 
						self.state.memory.load(_src,
									32))
			
		except:
			pass

		if(is_tainted(self.state) != None):# and not(_eval_int(self.state, self.state.ip) in memory_access)):
			print("---%s strncpy (dest=[%s], src=[%s...],  n=[%s])" % (repr(self.state),
					repr(_dest)[:30],
					repr(_src)[:30],
					repr(_n)[:30]))
				


			eval_args(self.state)
			if type(self.state.ip) != type(claripy.BVS("a",1)):
				_add_access(_eval_int(self.state,self.state.ip.to_claripy()))


			new_dest = claripy.BVS("hooked_arg_%s" % ((random.random()*999%64)), 8,annotations=(claripy.Annotation(),))
			return dest 

class strcpy_hook(angr.SimProcedure):
	def run(self, dest, src):

		#print("hook!")
		_src = src
		_dest = dest
		try:
			_src = _eval_int(self.state, src.to_claripy())
			_dest = hex(_eval_int(self.state, dest.to_claripy()))
			_src = _eval_bytes(self.state, self.state.memory.load(src,32))

		except:
			pass

		if(is_tainted(self.state) != None):# and not(_eval_int(self.state, self.state.ip) in memory_access)):
			print("---%s strcpy (dest=[%s], src=[%s...])" % (repr(self.state),
								repr(_dest)[:30],
								repr(_src)[:30]))
			

			eval_args(self.state)
	
			#_print_constraints(self.state)
			if type(self.state.ip) != type(claripy.BVS("a",1)):
				_add_access(_eval_int(self.state,self.state.ip.to_claripy()))
			else:
				_add_access(_eval_int(self.state,self.state.ip))

			dest = claripy.BVS("hooked_arg_%s" % ((random.random()*999%64)), 0x8,annotations=(claripy.Annotation(),))
			return dest 
		


class memmove_hook(angr.SimProcedure):
	def run(self, dest, src, n):


		_dest = dest
		_src = src
		_n = n

		try:
			_n = _eval_int(self.state, n.to_claripy())
			_src = _eval_int(self.state, src.to_claripy())
			_dest = hex(_eval_int(self.state, dest.to_claripy()))
			_src = _eval_bytes(self.state, self.state.memory.load(src,32))

		except:
			pass

		if(is_tainted(self.state) != None):# and not(_eval_int(self.state, self.state.ip) in memory_access)):
			print("---%s memmove (dest=[%s], src=[%s...], n=[%s])" % (repr(self.state),
							repr(_dest)[:30],
							repr(_src)[:30],
							repr(_n)[:30]))			
					

			eval_args(self.state)
			if type(self.state.ip) != type(claripy.BVS("a",1)):
				_add_access(_eval_int(self.state,self.state.ip.to_claripy()))
			else:
				_add_access(_eval_int(self.state,self.state.ip))

			dest = claripy.BVS("hooked_arg_%s" % ((random.random()*999%64)), 8,annotations=(claripy.Annotation(),))
			return dest 
		

class memset_hook(angr.SimProcedure):
	def run(self, s, c, n):
		_s = s
		try:
			_n = _eval_int(self.state, n.to_claripy())
			_s = self.state.solver.eval(self.state.memory.load(_s,32),cast_to=bytes)

		except:
			pass
		if(is_tainted(self.state) != None):# and not(_eval_int(self.state, self.state.ip) in memory_access)):
			print("---%s memset (s=[%s], c=[%s], n=[%s])" % (repr(self.state),
									repr(_s)[:30],
									repr(c)[:30],
									repr(_n)[:30]))

				
			eval_args(self.state)
			if type(self.state.ip) != type(claripy.BVS("a",1)):
				_add_access(_eval_int(self.state,self.state.ip.to_claripy()))
			else:
				_add_access(_eval_int(self.state,self.state.ip))

			dest = claripy.BVS("hooked_arg_%s" % ((random.random()*999%64)), 8,annotations=(claripy.Annotation(),))
			return dest 


class memcpy_hook(angr.SimProcedure):
	def run(self, dest, src, n):
		_dest = dest
		_src = src
		_n = n

		try:
			_dest = _eval_int(self.state, dest.to_claripy())
			_src = _eval_int(self.state, src.to_claripy())
			_n = _eval_int(self.state, n.to_claripy())

			_src = _eval_bytes(self.state,self.state.memory.load(src,32))

		except:
			pass

		if(is_tainted(self.state) != None):# not(_eval_int(self.state, self.state.ip) in memory_access)):
			print("---%s memcpy (dest=[%s], src=[%s...], n=[%s])" % (repr(self.state),
									repr(_dest)[:30],
									repr(_src)[:30],
									repr(_n)[:30]))			

				
			eval_args(self.state)
			if type(self.state.ip) != type(claripy.BVS("a",1)):
				_add_access(_eval_int(self.state,self.state.ip.to_claripy()))
			else:
				_add_access(_eval_int(self.state,self.state.ip))

			dest = claripy.BVS("hooked_arg_%s" % ((random.random()*999%64)), 
								8,
							annotations=(claripy.Annotation(),))
			return dest 
	
"""
class fscanf_hook():
class fgets_hook():
class sprintf_hook():
class snprintf_hook():
class vsprintf_hook():
class vnsprintf_hook():
class getch_hook():
getchar",getchar_hook())	
strcpy",strcpy_hook())	
strncpy",strncpy_hook())	
strlen",strlen_hook())	
strcat",strcat_hook())	
strncat",strncat_hook())	
strcmp",strcmp_hook())	
memcpy",memcpy_hook())	
memcmp",memcmp_hook())	
memmove",memmove_hook())	
memset",memset_hook())	
"""

